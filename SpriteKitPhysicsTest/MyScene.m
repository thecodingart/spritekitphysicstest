//
//  MyScene.m
//  SpriteKitPhysicsTest
//
//  Created by Brandon Levasseur on 11/10/13.
//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import "MyScene.h"

@implementation MyScene
{
    SKSpriteNode *_square;
    SKSpriteNode *_circle;
    SKSpriteNode *_triangle;
}

-(instancetype)initWithSize:(CGSize)size
{
    if (self = [super initWithSize:size]) {
        
        _square = [SKSpriteNode spriteNodeWithImageNamed:@"square"];
        _square.position = CGPointMake(self.size.width * 0.25, self.size.height * 0.50);
        [self addChild:_square];
        
        _circle = [SKSpriteNode spriteNodeWithImageNamed:@"circle"];
        _circle.position = CGPointMake(self.size.width * 0.50, self.size.height * 0.50);
        [self addChild:_circle];
        
        _triangle = [SKSpriteNode spriteNodeWithImageNamed:@"triangle"];
        _triangle.position = CGPointMake(self.size.width * 0.75, self.size.height * 0.50);
        [self addChild:_triangle];
        
        _square.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:_square.size];
        
        _circle.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:_circle.size.width /2];
        
        self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
        
        
        CGMutablePathRef trianglePath = CGPathCreateMutable();
        
        CGPathMoveToPoint(trianglePath, nil, -_triangle.size.width/2, -_triangle.size.height/2);
        
        CGPathAddLineToPoint(trianglePath, nil, _triangle.size.width/2, -_triangle.size.height/2);
        
        CGPathAddLineToPoint(trianglePath, nil, 0, _triangle.size.height/2);
        
        CGPathAddLineToPoint(trianglePath, nil, -_triangle.size.width/2, -_triangle.size.height/2);
        
        _triangle.physicsBody = [SKPhysicsBody bodyWithPolygonFromPath:trianglePath];
        
        CGPathRelease(trianglePath);
        
        
        [self runAction:
         [SKAction repeatAction:
          [SKAction sequence:
           @[[SKAction performSelector:@selector(spawnSand) onTarget:self],
             [SKAction waitForDuration:0.2]
             ]]
                          count:100]
         ];
        
    }
    return self;
}

-(void)spawnSand
{
    SKSpriteNode *sand = [SKSpriteNode spriteNodeWithImageNamed:@"sand"];
    sand.position = CGPointMake((float)(arc4random()%(int)self.size.width), self.size.height);
    sand.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:sand.size.width/2];
    sand.name = @"sand";
    [self addChild:sand];
}

@end
