//
//  AppDelegate.h
//  SpriteKitPhysicsTest
//
//  Created by Brandon Levasseur on 11/10/13.
//  Copyright (c) 2013 Brandon Levasseur. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
